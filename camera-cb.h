#ifndef __CAMERA_CB_H__
#define __CAMERA_CB_H__

#include <iostream>

// Tamaño de la ventana
int w = 800, h = 600;

// En caso de estar minimizado (false), no dibujar
bool dibuja = true;

// Escala de los objetos en relación con la ventana
float escala = 100, escala0;

// Seleccionar entre el modo perspectiva u ortogonal
bool perspectiva = false;

// Variables para la cámara
float eye[] = {0, 0, 10}, target[] = {0, 0, 0},
      up[] = {0, 1, 0};   // mirando hacia y vertical

// Planos de recorte
float znear = 0.001,
      zfar = 100;   // (en 5 => veo de 2 a -2)

// Ángulo de rotación del mundo alrededor del eje y
float ang_world, ang_world0;

// Rotación de los objetos de la escena
bool rota_obj = true;
float ang_obj;

// Mostrar stencils
bool stencil_draw = false;

// Activar o desactivar el recorte por stencil
bool stencil_test = true;

/**
 * Regenera la matriz de proyección cuando se rota
 * o cambia algún parámetro de la vista
 */
void regen() {
    if (!dibuja) return;

    // matriz de proyeccion
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    double w0 = (double)w / 2 / escala,
           h0 = (double)h / 2 / escala;   // semiancho y semialto en el target

    // frustum, perspective y ortho son respecto al eye pero con los z positivos
    // (delante del ojo)
    if (perspectiva) {   // perspectiva
        double   // "medio" al cuete porque aqui la distancia es siempre 5
            delta[3] = {(target[0] - eye[0]),   // vector ojo-target
                        (target[1] - eye[1]), (target[2] - eye[2])},
            dist = sqrt(delta[0] * delta[0] + delta[1] * delta[1] +
                        delta[2] * delta[2]);
        w0 *= znear / dist, h0 *= znear / dist;   // w0 y h0 en el near
        glFrustum(-w0, w0, -h0, h0, znear, zfar);
    } else {   // proyeccion ortogonal
        glOrtho(-w0, w0, -h0, h0, znear, zfar);
    }

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();   // matriz del modelo->view

    gluLookAt(eye[0], eye[1], eye[2], target[0], target[1], target[2], up[0],
              up[1],
              up[2]);   // ubica la camara

    glRotatef(ang_world, 0, 1, 0);   // rota los objetos alrededor de y

    glutPostRedisplay();
}

void Display_cb() {
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    if (stencil_test) {   // Activar o desactivar el test de stencil (para
                          // debuggear)
        glEnable(GL_STENCIL_TEST);
    } else {
        glDisable(GL_STENCIL_TEST);
    }

    // Dibujar la escena completa junto con los rebotes entre espejos
    drawAllScene();

    // Si está activada la opción para ver stencil y está activado el stencil
    // test
    if (stencil_test && stencil_draw) {
        // Rearmar el buffer de stencil
        redrawStencils();

        // Colorear los distintos valores
        // showStencil(0, 1, 1, 1);
        showStencil(1, 1, 0, 0);
        showStencil(2, 0, 1, 0);
        showStencil(3, 0, 0, 1);
        showStencil(4, 1, 1, 0);
        showStencil(5, 0, 1, 1);
        showStencil(6, 1, 0, 1);
    }

    glutSwapBuffers();

    // chequea errores
    int errornum = glGetError();
    while (errornum != GL_NO_ERROR) {
        if (errornum == GL_INVALID_ENUM)
            std::cout << "GL_INVALID_ENUM" << std::endl;
        else if (errornum == GL_INVALID_VALUE)
            std::cout << "GL_INVALID_VALUE" << std::endl;
        else if (errornum == GL_INVALID_OPERATION)
            std::cout << "GL_INVALID_OPERATION" << std::endl;
        else if (errornum == GL_STACK_OVERFLOW)
            std::cout << "GL_STACK_OVERFLOW" << std::endl;
        else if (errornum == GL_STACK_UNDERFLOW)
            std::cout << "GL_STACK_UNDERFLOW" << std::endl;
        else if (errornum == GL_OUT_OF_MEMORY)
            std::cout << "GL_OUT_OF_MEMORY" << std::endl;
        errornum = glGetError();
    }
}

void Idle_cb() {
    if (rota_obj) {            // Si la rotación está activa
        ang_obj += 1;          // Incrementar el ángulo
        if (ang_obj > 360) {   // Máximo de 360 grados
            ang_obj -= 360;
        }
        glutPostRedisplay();   // Mandar a renderizar
    }
}

void Reshape_cb(int width, int height) {
    h = height;   // Nuevo alto
    w = width;    // Nuevo ancho

    if (w == 0 || h == 0) {   // Si minimizó
        dibuja = false;       // no dibuja mas
        glutIdleFunc(0);      // no llama a cada rato a esa funcion
        return;
    } else if (!dibuja && w && h) {            // Si des-minimizó
        dibuja = true;                         // ahora si dibuja
        if (rota_obj) glutIdleFunc(Idle_cb);   // registra de nuevo el callback
    }

    glViewport(0, 0, w, h);   // Nuevas dimensiones

    regen();   // Regenerar la vista
}

#endif   // __CAMERA_CB_H__