#ifndef __MIRROR_H__
#define __MIRROR_H__

#include <GL/gl.h>
#include <GL/glut.h>
#include <glm/glm.hpp>

#include "Malla.h"

enum shape_e { SQUARE, TRIANGLE, ELLIPSE };

/**
 * Clase que representa un espejo
 */
class Mirror {
    /** Variable para almacenar la forma del espejo */
    shape_e shape;

    /** Matriz que representa la transformación del World Space
     * al Model Space del espejo
     */
    glm::mat4 mirrorT;

    /** Malla para almacenar el marco del espejo cuadrado */
    Malla *frame;

    /**
     * Dibuja un cuadrado de lado 2 unidades, centrado
     * en el origen
     */
    void drawShape(void);

    /**
     * Carga la malla correspondiente al marco del tipo
     * de espejo seleccionado
     */
    void loadFrameMesh(void);

   public:
    /**
     * Espejo a partir de la posición de su centro y un vector normal
     * hacia donde apunta y un tipo de forma dada. El tamaño es por defecto
     * @param position Posición del centro del espejo
     * @param normal Vector hacia donde apunta el espejo
     * @param shape Tipo de forma del espejo
     */
    Mirror(glm::vec3 position, glm::vec3 normal, shape_e shape = SQUARE);

    /**
     * Espejo a partir de la posición de su centro y un vector normal
     * hacia donde apunta y un tipo de forma dada. El tamaño está dado por
     * el ancho y el alto
     * @param position Posición del centro del espejo
     * @param normal Vector hacia donde apunta el espejo
     * @param shape Tipo de forma del espejo
     * @param width Ancho del espejo
     * @param height Alto del espejo
     */
    Mirror(glm::vec3 position, glm::vec3 normal, shape_e shape, float width,
           float height);

    /**
     * Espejo a partir de la transformación y el tipo de forma dada
     * @param modelT Matriz que representa la transformación desde el World
     * Space al Model Space del espejo
     * @param shape Tipo de forma del espejo
     */
    Mirror(glm::mat4 modelT, shape_e shape = SQUARE);

    /**
     * Espejo genérico en la posición original sobre el plano xy mirando
     * hacia -Z, cuadrado de 2 unidades de lado
     */
    Mirror();

    /**
     * Dibuja la superficie plana que ocupa el espejo, sin el marco, según
     * el tipo de forma que se haya seleccionado
     */
    void drawMirrorSurface(void);

    /**
     * Aplicar la transformación del World Space al Model Space del espejo.
     * Se aplica a la matriz seleccionada actualmente
     */
    void applyMirrorT(void);

    /**
     * Aplica la transformación para espejar la escena en la matriz
     * seleccionada actualmente. Es necesario aplicar la transformación al
     * Model Space del espejo previamente
     */
    void applyReflectionT(void);

    /**
     * Dibujar el marco del espejo según el tipo de forma que se haya
     * seleccionado
     */
    void drawFrame(void);
};

#endif   // __MIRROR_H__