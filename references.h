#ifndef __REFERENCIAS_H__
#define __REFERENCIAS_H__

#include <GL/gl.h>
#include <GL/glut.h>

/**
 * Dibuja los ejes coordenados X (en color rojo), Y (en
 * color verde) y Z (en color azul) partiendo del origen
 * con longitud 1 unidad
 */
void drawEjes() {
    glPushAttrib(GL_ALL_ATTRIB_BITS);

    // ejes
    glLineWidth(4);

    // set_material(MT_None);
    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3d(0, 0, 0);
    glVertex3d(1, 0, 0);
    glColor3f(0, 1, 0);
    glVertex3d(0, 0, 0);
    glVertex3d(0, 1, 0);
    glColor3f(0, 0, 1);
    glVertex3d(0, 0, 0);
    glVertex3d(0, 0, 1);
    glEnd();

    glPopAttrib();
}

/**
 * Dibuja un cubo de lado 2 unidades centrado en el
 * origen
 */
void drawCube() {
    glPushAttrib(GL_ALL_ATTRIB_BITS);

    glLineWidth(1);

    // set_material(MT_None);
    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    glColor4f(.0, .0, .0, 1);

    glBegin(GL_LINES);
    glVertex3f(-1, -1, -1);
    glVertex3f(1, -1, -1);
    glVertex3f(-1, -1, -1);
    glVertex3f(-1, 1, -1);
    glVertex3f(-1, -1, -1);
    glVertex3f(-1, -1, 1);
    glVertex3f(1, 1, 1);
    glVertex3f(1, 1, -1);
    glVertex3f(1, 1, 1);
    glVertex3f(1, -1, 1);
    glVertex3f(1, 1, 1);
    glVertex3f(-1, 1, 1);
    glVertex3f(-1, -1, 1);
    glVertex3f(1, -1, 1);
    glVertex3f(-1, -1, 1);
    glVertex3f(-1, 1, 1);
    glVertex3f(-1, 1, -1);
    glVertex3f(-1, 1, 1);
    glVertex3f(-1, 1, -1);
    glVertex3f(1, 1, -1);
    glVertex3f(1, -1, -1);
    glVertex3f(1, 1, -1);
    glVertex3f(1, -1, -1);
    glVertex3f(1, -1, 1);
    glEnd();

    glPopAttrib();
}

#endif   // __REFERENCIAS_H__