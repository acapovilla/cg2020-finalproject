#ifndef __AUX_CB_H__
#define __AUX_CB_H__

#include <cmath>
#include <iostream>

#include <GL/gl.h>
#include <GL/glut.h>

/**
 * Manejo de teclado y mouse
 */
short modifiers = 0;              // ctrl, alt, shift (de GLUT)
int boton = -1, xclick, yclick;   // x e y cuando clickeo un boton
static const double R2G = 45 / atan(1.0);

float ac0,
    rc0;   // angulo resp x y distancia al target de la camara al clickear

/**
 * Variables de estado de la ejecución
 */
// Material
extern int material_idx;

// Dibujar las referencias
extern bool references;

// Rotación de los objetos de la escena
extern bool rota_obj;

// Mostrar los stencil
extern bool stencil_draw;

// Activar o desactivar el recorte por stencil
extern bool stencil_test;

// Iteraciones en la recursion
extern int max_recursions;

//------------------------------------------------------------
// Teclado
/*
GLUT ACTIVE SHIFT //Set if the Shift modifier or Caps Lock is active.
GLUT ACTIVE CTRL //Set if the Ctrl modifier is active.
GLUT ACTIVE ALT //Set if the Alt modifier is active.
*/
inline short get_modifiers() { return modifiers = (short)glutGetModifiers(); }

//------------------------------------------------------------
// Movimientos del mouse
void Motion_cb(int xm, int ym) {   // drag
    if (boton == GLUT_LEFT_BUTTON) {
        if (modifiers == GLUT_ACTIVE_SHIFT) {   // cambio de escala
            escala = escala0 * exp((yclick - ym) / 100.0);
            regen();
        } else {   // manipulacion
            double yc = eye[1] - target[1], zc = eye[2] - target[2];
            double ac = ac0 + (ym - yclick) * 180.0 / h / R2G;
            yc = rc0 * sin(ac);
            zc = rc0 * cos(ac);
            up[1] = zc;
            up[2] = -yc;   // perpendicular
            eye[1] = target[1] + yc;
            eye[2] = target[2] + zc;
            ang_world = ang_world0 + (xm - xclick) * 180.0 / w;
            regen();
        }
    }
}

void Keyboard_cb(unsigned char key, int x = 0, int y = 0) {
    switch (key) {
        case '+':   // Aumentar iteraciones
            if (max_recursions < 5) {
                ++max_recursions;
                std::cout << "Recursion levels: " << max_recursions + 1
                          << std::endl;
            }
            break;
        case '-':   // Disminuir iteraciones
            if (max_recursions > 0) {
                --max_recursions;
                std::cout << "Recursion levels: " << max_recursions + 1
                          << std::endl;
            }
            break;
        case 'e':
        case 'E':   // Ejes de referencia
            references = !references;
            break;
        case 'p':
        case 'P':   // Perspectiva / Ortogonal
            perspectiva = !perspectiva;
            regen();
            break;
        case 'r':
        case 'R':   // Rotación del objeto
            rota_obj = !rota_obj;
            break;
        case 'm':
        case 'M':   // Cambio de material
            if (key == 'm')
                ++material_idx;
            else
                --material_idx;
            break;
        case 't':
        case 'T':   // test de stencil
            stencil_test = !stencil_test;
            break;
        case 's':
        case 'S':   // ver stencils
            stencil_draw = !stencil_draw;
            break;
        // Cambios de escena
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
            changeScene(key);
            break;
        case 27:   // escape => exit
            exit(EXIT_SUCCESS);
            break;
    }
    glutPostRedisplay();
}

void Special_cb(int key, int xm = 0, int ym = 0) {
    if (key == GLUT_KEY_F4) {   // alt+f4 => exit
        get_modifiers();
        if (modifiers == GLUT_ACTIVE_ALT) exit(EXIT_SUCCESS);
    }
}

void Menu_cb(int value) {
    if (value < 256)
        Keyboard_cb(value);
    else
        Special_cb(value - 256);
}

void createMenu(void) {
    glutCreateMenu(Menu_cb);
    // Escenas
    glutAddMenuEntry("         Escenas: Numerales     ", '\0');
    // Iteraciones de la recusividad
    glutAddMenuEntry("     [+]_Aumentar iteraciones   ", '+');
    glutAddMenuEntry("     [-]_Disminuir iteraciones  ", '-');
    // Activar o desactivar el recorte de stencil
    glutAddMenuEntry("     [t]_Test de stencil        ", 't');
    // Activar o desactivar el recorte de stencil
    glutAddMenuEntry("     [s]_Ver stencil buffer     ", 's');
    // Modo de visión.
    glutAddMenuEntry("     [p]_Perspectiva/Ortogonal  ", 'p');
    // Rotación del objeto
    glutAddMenuEntry("     [r]_Rota                   ", 'r');
    // Visualización de los ejes de referencia
    glutAddMenuEntry("     [e]_Ver ejes               ", 'e');
    // Cambio de material
    glutAddMenuEntry("     [m]_Material               ", 'm');
    glutAddMenuEntry("   [Esc]_Exit                   ", 27);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

// Clicks del mouse
// GLUT LEFT BUTTON, GLUT MIDDLE BUTTON, or GLUT RIGHT BUTTON
// The state parameter is either GLUT UP or GLUT DOWN
// glutGetModifiers may be called to determine the state of modifier keys
void Mouse_cb(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_DOWN) {
            xclick = x;
            yclick = y;
            boton = button;
            get_modifiers();
            glutMotionFunc(Motion_cb);              // callback para los drags
            if (modifiers == GLUT_ACTIVE_SHIFT) {   // cambio de escala
                escala0 = escala;
            } else {   // manipulacion
                double yc = eye[1] - target[1], zc = eye[2] - target[2];
                rc0 = sqrt(yc * yc + zc * zc);
                ac0 = atan2(yc, zc);
                ang_world0 = ang_world;
            }
        } else if (state == GLUT_UP) {
            boton = -1;
            glutMotionFunc(0);   // anula el callback para los drags
        }
    }
}

#endif   // __AUX_CB_H__