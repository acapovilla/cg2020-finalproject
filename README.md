# Reflexiones planas e Interreflexiones

Trabajo final para la materia Computación Gráfica (2020) de la carrera de Ingeniería Informática de la Facultad de Ingeniería y Ciencias Hídricas (FICH) de la Universidad Nacional del Litoral  (UNL).

## Alumnos

- Adjadj, Agustín
- Capovilla, Agustín
- Redi, Ulises

![](images/scene1.png)

## Notes

Modificar el archivo ```files-path.h``` según el sistema operativo que se utilice para acceder a los archivos de las mallas.