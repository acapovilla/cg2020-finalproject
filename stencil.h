#ifndef __STENCIL_H__
#define __STENCIL_H__

#include <GL/glut.h>

/**
 * Pinta la pantalla según un color dado para un valor dado del
 * buffer de stencil (Utilizado para debuggear)
 * @param val Valor de comparación con el buffer
 * @param r Valor del canal rojo
 * @param g Valor del canal verde
 * @param b Valor del canal Azul
 */
void showStencil(unsigned char val, float r, float g, float b) {
    glPushAttrib(GL_ALL_ATTRIB_BITS);   // No modificar los atributos ya cargados

    // Comparar todos los bits con el valor de val
    glStencilFunc(GL_EQUAL, val, ~0x00);

    // Cargar la identidad en la matriz Proyección
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    // Cargar la identidad en la matriz Vista-Modelo
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    // Deshabilitar la mezcla y la iluminación
    glEnable(GL_BLEND);
    glDisable(GL_LIGHTING);

    // Setear el color dado con transparencia para poder ver la escena
    glColor4f(r, g, b, 0.3);

    // Dibujar un quad que "ocupe toda la pantalla"
    glBegin(GL_QUADS);
    glVertex3f(-1, -1, -1);
    glVertex3f(1, -1, -1);
    glVertex3f(1, 1, -1);
    glVertex3f(-1, 1, -1);
    glEnd();

    // Volver a la matriz original en la Vista-Modelo
    glPopMatrix();

    // Volver a la matriz original en la Proyección
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glPopAttrib();   // Volver a la configuración de atributos original
}

#endif   // __STENCIL_H__