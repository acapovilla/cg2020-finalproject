#include "Mirror.h"

#include <iostream>
#include <cmath>

#include <GL/gl.h>
#include <GL/glut.h>

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Malla.h"
#include "materials.h"
#include "files-path.h"

Mirror::Mirror(glm::vec3 position, glm::vec3 normal, shape_e shape) {
    Mirror(position, normal, shape, 1.0f, 1.0f);
}

Mirror::Mirror(glm::vec3 position, glm::vec3 normal, shape_e shape, float width,
               float height) {
    // Translate(position)
    // Scale(width, heigth)
    // RotateTo(normal)
}

Mirror::Mirror(glm::mat4 modelT, shape_e shape) {
    mirrorT = glm::mat4(modelT);
    (*this).shape = shape;

    loadFrameMesh();
}

Mirror::Mirror() {
    mirrorT = glm::mat4(1.0f);
    shape = SQUARE;

    loadFrameMesh();
}

/** Dibujar un cuadrado de lado 2, centrado en el origen apuntando hacia z
 * negativo */
void drawSquare(void) {
    glBegin(GL_QUADS);
    glNormal3f(0, 0, -1);
    glVertex3f(-1, -1, 0);
    glVertex3f(1, -1, 0);
    glVertex3f(1, 1, 0);
    glVertex3f(-1, 1, 0);
    glEnd();
}

void Mirror::drawShape(void) {
    switch (shape) {
        case SQUARE:
            drawSquare();
            break;

        case TRIANGLE:
            // drawTriangle();
            break;

        case ELLIPSE:
            // drawEllipse();
            break;
    }
}

void Mirror::loadFrameMesh(void) {
    switch (shape) {
        case SQUARE:
            frame = new Malla(SQUARE_FRAME_MESH);
            break;

        case TRIANGLE:
            frame = new Malla(TRIANGLE_FRAME_MESH);
            break;

        case ELLIPSE:
            frame = new Malla(ELLIPSE_FRAME_MESH);
            break;
    }
}

void Mirror::drawMirrorSurface(void) { drawShape(); }

void Mirror::applyMirrorT(void) { glMultMatrixf(glm::value_ptr(mirrorT)); }

void Mirror::applyReflectionT(void) {
    // Calcular la transformación de reflección
    glm::mat4 reflectionT =
        glm::scale(glm::mat4(1.0),
                   glm::vec3(1.f, 1.f, -1.f)) *   // Householder para -z
        glm::inverse(mirrorT);

    // Aplicar la transformación
    glMultMatrixf(glm::value_ptr(reflectionT));
}

void Mirror::drawFrame(void) {
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    {
        // Setear el material del marco
        set_material(GL_FRONT_AND_BACK, 10);
        glPushMatrix();
        {
            // Aplicar la transformación al Model Space del espejo
            applyMirrorT();
            // Dibujar la malla del marco
            (*frame).Draw();
        }
        glPopMatrix();
    }
    glPopAttrib();
}