#ifndef __MATRIX_STACK__
#define __MATRIX_STACK__

#include <vector>

#include <GL/gl.h>
#include <GL/glut.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

/** Stack auxiliar de matrices */
std::vector<glm::mat4> stack;

/**
 * Agrega la matriz actual del model view al stack auxiliar
 */
void pushMatrix() {
    GLfloat model[16];
    glGetFloatv(GL_MODELVIEW_MATRIX, model);

    stack.push_back(glm::make_mat4(model));
}

/**
 * Aplica al model view la matriz del stack auxiliar y la
 * elimina
 */
void popMatrix() {
    glLoadMatrixf(glm::value_ptr(stack.back()));
    stack.pop_back();
}

#endif   // __MATRIX_STACK__