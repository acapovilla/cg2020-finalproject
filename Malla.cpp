#include <iostream>
#include <fstream>

#include <GL/glut.h>

#include "Malla.h"
using namespace std;

void Malla::Load(const char *fname) {
    e.clear();
    p.clear();
    ifstream f(fname);
    if (!f.is_open()) return;
    int i, nv;
    f >> nv;
    float x, y, z;
    for (i = 0; i < nv; i++) {
        f >> x >> y >> z;
        p.push_back(Nodo(x, y, z));
    }
    int ne;
    f >> ne;
    int v0, v1, v2, v3;
    for (i = 0; i < ne; i++) {
        f >> nv >> v0 >> v1 >> v2;
        if (nv == 3) {
            AgregarElemento(v0, v1, v2);
        } else {
            f >> v3;
            AgregarElemento(v0, v1, v2, v3);
        }
    }
    f.close();
    MakeVecinos();
    MakeNormales();
}

Malla::Malla(const char *fname) {
    if (fname) Load(fname);
}

void Malla::Save(const char *fname) {
    ofstream f(fname);
    f << p.size() << endl;
    unsigned int i, j;
    for (i = 0; i < p.size(); i++)
        f << p[i].x[0] << ' ' << p[i].x[1] << ' ' << p[i].x[2] << endl;
    f << e.size() << endl;
    for (i = 0; i < e.size(); i++) {
        f << e[i].nv;
        for (j = 0; j < e[i].nv; j++) f << ' ' << e[i][j];
        f << endl;
    }
    f.close();
}

void Malla::AgregarElemento(int n0, int n1, int n2, int n3) {
    int ie = e.size();
    e.push_back(Elemento(n0, n1, n2, n3));   // agrega el Elemento
    // avisa a cada nodo que ahora es vertice de este elemento
    p[n0].e.push_back(ie);
    p[n1].e.push_back(ie);
    p[n2].e.push_back(ie);
    if (n3 >= 0) p[n3].e.push_back(ie);
}

void Malla::ReemplazarElemento(int ie, int n0, int n1, int n2, int n3) {
    Elemento &ei = e[ie];
    // estos nodos ya no seran vertices de este elemento
    for (unsigned int i = 0; i < ei.nv; i++) {
        vector<int> &ve = p[ei[i]].e;
        ve.erase(find(ve.begin(), ve.end(), ie));
    }
    ei.SetNodos(n0, n1, n2, n3);
    // estos nodos ahora son vertices
    p[n0].e.push_back(ie);
    p[n1].e.push_back(ie);
    p[n2].e.push_back(ie);
    if (n3 >= 0) p[n3].e.push_back(ie);
}

void Malla::MakeNormales() {
    normal.resize(p.size());
    for (unsigned int i = 0; i < p.size();
         i++) {   // "promedio" de normales de cara
        vector<int> &en = p[i].e;
        Punto n(0, 0, 0);
        int k;
        for (unsigned int j = 0; j < en.size(); j++) {
            Elemento &ej = e[en[j]];
            k = ej.Indice(i);
            n += (p[ej[k]] - p[ej[k - 1]]) % (p[ej[k + 1]] - p[ej[k]]);
        }
        float m = n.mod();
        if (m > 1e-10)
            n /= m;
        else
            n.zero();
        normal[i] = n;
    }
}

void Malla::Draw(bool relleno) {
    // dibuja los Elementos
    unsigned int i;
    if (relleno) {
        glEnable(GL_LIGHTING);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    } else {
        glDisable(GL_LIGHTING);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    for (i = 0; i < e.size(); i++) {
        if (e[i].nv == 4) {
            glBegin(GL_QUADS);
            glNormal3fv(normal[e[i][0]].x);
            glVertex3fv(p[e[i][0]].x);
            glNormal3fv(normal[e[i][1]].x);
            glVertex3fv(p[e[i][1]].x);
            glNormal3fv(normal[e[i][2]].x);
            glVertex3fv(p[e[i][2]].x);
            glNormal3fv(normal[e[i][3]].x);
            glVertex3fv(p[e[i][3]].x);
        } else {
            glBegin(GL_TRIANGLES);
            glNormal3fv(normal[e[i][0]].x);
            glVertex3fv(p[e[i][0]].x);
            glNormal3fv(normal[e[i][1]].x);
            glVertex3fv(p[e[i][1]].x);
            glNormal3fv(normal[e[i][2]].x);
            glVertex3fv(p[e[i][2]].x);
        }
        glEnd();
    }
    // dibuja solo los nodos sueltos
    glDisable(GL_LIGHTING);
    glBegin(GL_POINTS);
    for (i = 0; i < p.size(); i++) {
        if (p[i].e.size() == 0) glVertex3fv(p[i].x);
    }
    glEnd();
}

// Identifica los pares de elementos vecinos y las aristas de frontera
// Actualiza el atributo v (lista de vecinos) de cada elemento y el atributo
// es_frontera de cada nodo
void Malla::MakeVecinos() {
    unsigned int i, j, k, ie, iev;
    int ix;   // puede dar -1
    // inicializa
    for (i = 0; i < p.size(); i++)
        p[i].es_frontera =
            false;   // le dice a todos los nodos que no son frontera
    for (i = 0; i < e.size(); i++)
        e[i].v[0] = e[i].v[1] = e[i].v[2] = e[i].v[3] =
            -1;   // le dice a todos los elementos que no tienen vecinos
    // identificacion de vecinos
    for (ie = 0; ie < e.size(); ie++) {      // por cada elemento
        for (j = 0; j < e[ie].nv; j++) {     // por cada arista
            if (e[ie].v[j] >= 0) continue;   // ya se hizo
            int in0 = e[ie][j],
                in1 = e[ie][j + 1];   // 1er y 2do nodo de la arista
            for (k = 0; k < p[in0].e.size();
                 k++) {   // recorro los elementos del primer nodo
                iev = p[in0].e[k];
                if (iev == ie) continue;   // es este mismo
                // se fija si tiene a in1 (el 2do nodo)
                ix = e[iev].Indice(in1);
                if (ix < 0) continue;
                // tiene al 2do
                e[ie].v[j] = p[in0].e[k];   // ese es el vecino
                e[iev].v[ix] = ie;
                break;   // solo dos posibles vecinos para una arista
            }
            if (k == p[in0].e.size())   // no encontro vecino ==> frontera
                p[in0].es_frontera = p[in1].es_frontera = true;
        }
    }
}

void Malla::Subdivide() {
    int Nn = p.size(), Ne = e.size();

    for (int i = 0; i < Ne; ++i) {   // Por cada elemento
                                     // Agregar centroides
        float x = (p[e[i][0]].x[0] + p[e[i][1]].x[0] + p[e[i][2]].x[0]);
        float y = (p[e[i][0]].x[1] + p[e[i][1]].x[1] + p[e[i][2]].x[1]);
        float z = (p[e[i][0]].x[2] + p[e[i][1]].x[2] + p[e[i][2]].x[2]);

        float p_qty = 3;
        if (e[i].nv == 4) {   // Para cuadrilateros
            x += p[e[i][3]].x[0];
            y += p[e[i][3]].x[1];
            z += p[e[i][3]].x[2];

            p_qty = 4;
        }

        p.push_back(Nodo(x / p_qty, y / p_qty, z / p_qty));
    }

    // Nuevos puntos asociados a cada arista
    Mapa m;
    for (int i = 0; i < Ne; ++i) {             // Por cada elemento
        for (uint j = 0; j < e[i].nv; ++j) {   // Por cada cara vecina
            // Si la arista tiene vecinos y ya se analizó pasar
            if (e[i].v[j] > -1 && e[i].v[j] < i) continue;

            float x = (p[e[i][j]].x[0] + p[e[i][j + 1]].x[0]);
            float y = (p[e[i][j]].x[1] + p[e[i][j + 1]].x[1]);
            float z = (p[e[i][j]].x[2] + p[e[i][j + 1]].x[2]);

            float p_qty = 2;

            if (e[i].v[j] != -1) {    // Si la arista tiene vecinos
                x += p[Nn + i].x[0]   // el centroide de la cara actual
                     + p[Nn + e[i].v[j]]
                           .x[0];   // y el centroide de la cara vecina

                y += +p[Nn + i].x[1]   // el centroide de la cara actual
                     + p[Nn + e[i].v[j]]
                           .x[1];   // y el centroide de la cara vecina

                z += p[Nn + i].x[2]   // el centroide de la cara actual
                     + p[Nn + e[i].v[j]]
                           .x[2];   // y el centroide de la cara vecina

                p_qty = 4;
            }

            m[Arista(e[i], j)] = p.size();
            p.push_back(Nodo(x / p_qty, y / p_qty, z / p_qty));
        }
    }

    typedef struct {
        int n0, n1, n2, n3;
    } e4i;

    vector<e4i> new_elem;   // Auxiliar para acumular los elementos nuevos
    for (int i = 0; i < Ne; ++i) {             // Por cada elemento
        for (uint j = 0; j < e[i].nv; ++j) {   // Por cada vertice
            // centroide de la "primera" arista: Punto actual + siguiente
            int c1 = m[Arista(e[i][j], e[i][j + 1])];

            // siguiente al punto actual -> e[i][j + 1]

            // centroide de la "segunda" arista: Punto siguiente al actual +
            // siguiente
            int c2 = m[Arista(e[i][j + 1], e[i][j + 2])];

            // Centroide de la cara -> Nn + i

            // Añadir la nueva entidad en el vector auxiliar
            new_elem.push_back({c1, e[i][j + 1], c2, Nn + i});
        }

        ReemplazarElemento(i, new_elem[0].n0, new_elem[0].n1, new_elem[0].n2,
                           new_elem[0].n3);
        for (uint k = 1; k < new_elem.size(); ++k) {
            AgregarElemento(new_elem[k].n0, new_elem[k].n1, new_elem[k].n2,
                            new_elem[k].n3);
        }

        new_elem.clear();
    }

    MakeVecinos();

    for (int i = 0; i < Nn; ++i) {   // Para todos los nodos originales
        int puntos_f = 0, puntos_r = 0;
        Nodo r(0, 0, 0), f(0, 0, 0);

        for (uint j = 0; j < p[i].e.size();
             ++j) {   // Para cada entidad a la que pertenece
            // Obtener las aristas a las que pertenece el punto
            Elemento &e_aux = e[p[i].e[j]];   // Elemento actual: e[p[i].e[j]]
            int idx_v = e_aux.Indice(
                i);   // Indice en el que está el punto actual: .Indice(i)

            // Aristas:
            //      - A1: anterior + actual -> el vecino es idx_actual - 1
            int vecino_A1 = e_aux.v[(idx_v + e_aux.nv - 1) % e_aux.nv];
            //      - A2: actual + siguiente -> el vecino es idx_actual
            int vecino_A2 = e_aux.v[idx_v];

            // Filtro para A1
            if (vecino_A1 ==
                    -1 ||   // Si vecino -1, se analiza 1 sola vez -> Agregar
                vecino_A1 > p[i].e[j]) {   // Si el vecino es >, no se analizó
                                           // -> Agregar
                r += p[e_aux[idx_v + e_aux.nv - 1]];   // Agregar punto
                                                       // Anterior
                ++puntos_r;   // Incrementar el divisor para el promedio
            }

            // Filtro para A2
            if (vecino_A2 ==
                    -1 ||   // Si vecino -1, se analiza 1 sola vez -> Agregar
                vecino_A2 > p[i].e[j]) {    // Si el vecino es >, no se analizó
                                            // -> Agregar
                r += p[e_aux[idx_v + 1]];   // Agregar punto Siguiente
                ++puntos_r;   // Incrementar el divisor para el promedio
            }

            // Si no es forntera se debe computar los centroides de cara
            // El centroide de la cara es el punto opuesto en la entidad
            if (!p[i].es_frontera) {
                // !!!!!!! SUPUESTO DE QUE ES UN QUAD
                f += p[e_aux[idx_v + 2]];
                ++puntos_f;
            }
        }

        // Dividir y Mover punto
        if (p[i].es_frontera) {
            r /= puntos_r;
            p[i] = (r + p[i]) / 2;
        } else {
            r /= puntos_r;
            f /= puntos_f;
            p[i] = (r * 4 - f + p[i]) / 4;
        }
    }

    MakeNormales();
}
