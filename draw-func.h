#ifndef __DRAW_FUNC_H__
#define __DRAW_FUNC_H__

#include <vector>
#include <cmath>

#include <GL/gl.h>
#include <GL/glut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "references.h"
#include "Mirror.h"
#include "Malla.h"
#include "materials.h"
#include "files-path.h"

// #define USE_AUX_STACK

#ifdef USE_AUX_STACK
#include "matrix-stack.h"
#endif

typedef std::vector<Mirror> Mirrors;

// Dibujar las referencias
bool references = true;

// Angulo de rotación para los objetos
extern float ang_obj;

float lpos[4] = {.0f, 2.f, 0.0f, 0.f};   // posicion luz

// Índice del material a aplicar a los objetos
int material_idx = 0;

// Cantidad máxima de recursiones
int max_recursions = 5;

// Plano de clipping para los espejos
glm::dvec4 clipplingPlane = glm::dvec4(0, 0, -1, 0);

// Malla para la mona
Malla* susan;

// Vector de espejos que componen la escena
Mirrors mirrors;

/** -------------------------  Espejos de ejemplo ------------------------- **/
glm::mat4 mirror1_m =                                                      //
    glm::rotate(glm::mat4(1.f), glm::radians(0.f), glm::vec3(0, 1, 0)) *   //
    glm::scale(glm::mat4(1.f), glm::vec3(2, 2, 1)) *                       //
    glm::translate(glm::mat4(1), glm::vec3(0, 0, -2.2))                    //
    ;

glm::mat4 mirror2_m =                                                        //
    glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0, 1, 0)) *   //
    glm::scale(glm::mat4(1.f), glm::vec3(2, 2, 1)) *                         //
    glm::translate(glm::mat4(1), glm::vec3(0, 0, -2.2))                      //
    ;

glm::mat4 mirror3_m =                                                        //
    glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(1, 0, 0)) *   //
    glm::scale(glm::mat4(1.f), glm::vec3(2, 2, 1)) *                         //
    glm::translate(glm::mat4(1), glm::vec3(0, 0, -2.2))                      //
    ;

glm::mat4 mirror4_m =                                                        //
    glm::rotate(glm::mat4(1.f), glm::radians(195.f), glm::vec3(0, 1, 0)) *   //
    glm::scale(glm::mat4(1.f), glm::vec3(2, 2, 1)) *                         //
    glm::translate(glm::mat4(1), glm::vec3(0, 0, -2.3))                      //
    ;

glm::mat4 mirror5_m =                                                        //
    glm::rotate(glm::mat4(1.f), glm::radians(-45.f), glm::vec3(0, 1, 0)) *   //
    glm::scale(glm::mat4(1.f), glm::vec3(2, 2, 1)) *                         //
    glm::translate(glm::mat4(1), glm::vec3(0, 0, -2.3))                      //
    ;

glm::mat4 mirror6_m =                                                        //
    glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0, 1, 0)) *   //
    glm::scale(glm::mat4(1.f), glm::vec3(2, 2, 1)) *                         //
    glm::translate(glm::mat4(1), glm::vec3(0, 0, -5))                        //
    ;

glm::mat4 mirror7_m =                                                      //
    glm::rotate(glm::mat4(1.f), glm::radians(0.f), glm::vec3(0, 1, 0)) *   //
    glm::scale(glm::mat4(1.f), glm::vec3(2, 2, 1)) *                       //
    glm::translate(glm::mat4(1), glm::vec3(0, 0, -1.1))                    //
    ;

glm::mat4 mirror8_m =   //
    glm::rotate(glm::mat4(1.f), glm::radians(225.f + 7.5f),
                glm::vec3(0, 1, 0)) *                     //
    glm::scale(glm::mat4(1.f), glm::vec3(2, 2, 1)) *      //
    glm::translate(glm::mat4(1), glm::vec3(0, 0, -1.1))   //
    ;

Mirror mirror1 = Mirror(mirror1_m), mirror2 = Mirror(mirror2_m),
       mirror3 = Mirror(mirror3_m), mirror4 = Mirror(mirror4_m),
       mirror5 = Mirror(mirror5_m), mirror6 = Mirror(mirror6_m),
       mirror7 = Mirror(mirror7_m), mirror8 = Mirror(mirror8_m);

/** ----------------------------------------------------------------------- **/

/**
 * Función para dibujar la mona susan
 */
void drawSusan(float rotacion) {
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    {
        // Setear el material según el que se encuentra elegido
        set_material(GL_FRONT_AND_BACK, material_idx);

        glLightfv(GL_LIGHT0, GL_POSITION, lpos);   // ubica la luz

#ifdef USE_AUX_STACK
        pushMatrix();
#else
        glPushMatrix();
#endif
        {
            // Transformación para la rotación del modelo
            glRotatef(rotacion, 0.0, 1.0, 0.0);
            (*susan).Draw(true);   // Dibujar la malla
        }
#ifdef USE_AUX_STACK
        pushMatrix();
#else
        glPopMatrix();
#endif
    }
    glPopAttrib();
}

/**
 * Dibuja un suelo cuadrado sobre el plano xz seǵun la dimensión de los lados y
 * una altura medida desde el origen sobre el eje y. Se dibuja de ambos lados,
 * mirando hacia y negativo e y positivo.
 * @param size Longitud del lado en unidades
 * @param height Altura en el eje y
 */
void drawFloor(float size, float height) {
    constexpr float step = 4 / 24.f;   // Salto para general la cuadrícula

    // Como el bucle va desde -size a size, la longitud sería de 2 * size,
    // por lo que se debe dividir a la mitad
    size = size / 2;

    glBegin(GL_QUADS);
    for (float x = -size; x < size - step / 2; x += step) {
        for (float z = -size; z < size - step / 2; z += step) {
            // Lado de arriba
            glNormal3f(0, 1, 0);
            glVertex3f(x, height, z + step);
            glVertex3f(x + step, height, z + step);
            glVertex3f(x + step, height, z);
            glVertex3f(x, height, z);
            // Lado de abajo
            glNormal3f(0, -1, 0);
            glVertex3f(x, height, z);
            glVertex3f(x + step, height, z);
            glVertex3f(x + step, height, z + step);
            glVertex3f(x, height, z + step);
        }
    }
    glEnd();
}

/**
 * Dibujar la escena
 */
void drawScene(void) {
    if (references) {   // Si están activadas las referencias, dibujarlas
        // drawCube();
        drawEjes();
    }

// Dibuja un icosaedro como figura para debugging
#ifdef USE_AUX_STACK
    pushMatrix();
#else
    glPushMatrix();
#endif
    {
        glTranslatef(0, 0, -5);
        glutSolidIcosahedron();
    }
#ifdef USE_AUX_STACK
    pushMatrix();
#else
    glPopMatrix();
#endif

// Dibuja una esfera para debugging
#ifdef USE_AUX_STACK
    pushMatrix();
#else
    glPushMatrix();
#endif
    {
        glTranslatef(5, 0, 0);
        glutSolidSphere(1, 10, 10);
    }
#ifdef USE_AUX_STACK
    pushMatrix();
#else
    glPopMatrix();
#endif

    // Dibuja a susan (modelo principal)
    drawSusan(ang_obj);

    // Dibujar el suelo cuadrado, de lado 12 unidades y ubicado 3 unidades
    // debajo del origen
    drawFloor(12, -3);

    // Bucle para dibujar los marcos de todos los espejos que componen la escena
    for (size_t i = 0; i < mirrors.size(); ++i) {
        mirrors[i].drawFrame();
    }
}

/**
 * Función recursiva para redibujar las máscaras de los espejos.
 * @param mirrors Vector de espejos que componen la escena
 * @param current Índice dentro del vector del espejo actual en el cuál se está
 * reflejando
 * @param recursion_lvl Nivel de recursión actual
 */
void redrawStencils(Mirrors mirrors, size_t current, int recursion_lvl) {
    int cullfacing = recursion_lvl % 2;   // Orientación para el espejo

    for (size_t i = 0; i < mirrors.size();
         ++i) {   // Para cada espejo en la escena
        if (i == current)
            continue;   // Si es el actual, no se refleja a sí mismo

        glPushAttrib(GL_ALL_ATTRIB_BITS);
        {
            // Setear el test de stencil y las funciones
            glStencilFunc(GL_LESS, recursion_lvl - 2, 0xFF);
            glStencilOp(GL_KEEP, GL_INCR, GL_INCR);

            glFrontFace(GL_CW + cullfacing);   // Orientación para el espejo

#ifdef USE_AUX_STACK
            pushMatrix();
#else
            glPushMatrix();
#endif
            {
                // Aplicar la transformación para el Model Space del espejo
                mirrors[i].applyMirrorT();
                // Dibujar el espejo (sin marco)
                mirrors[i].drawMirrorSurface();

                // Orientación inversa para la escena reflejada
                glFrontFace(GL_CCW - cullfacing);
                mirrors[i]
                    .applyReflectionT();   // Aplicar la reflexión del espejo

                if (recursion_lvl <
                    max_recursions) {   // Si no se llegó al nivel máximo
                    // Lanzar otro nivel de recursión
                    redrawStencils(mirrors, i, recursion_lvl + 1);
                }
            }
#ifdef USE_AUX_STACK
            pushMatrix();
#else
            glPopMatrix();
#endif
        }
        glPopAttrib();
    }
}

/**
 * Función para redibujar los stencil. Debido a que la técnica redibuja los
 * espejos en modo decrementar, al finalizar todas las iteraciones de la
 * recursión, el buffer de stencil queda limpi y no se puede visualizar ningún
 * stencil intermedio. Por lo tanto esta función recorre el mismo árbol de
 * recursión, pero sin despintar el buffer para que se puedan ver.
 */
void redrawStencils(void) {
    for (size_t i = 0; i < mirrors.size();
         ++i) {   // Para cada espejo en la escena
        glPushAttrib(GL_ALL_ATTRIB_BITS);
        {
            glColorMask(GL_FALSE, GL_FALSE, GL_FALSE,
                        GL_FALSE);   // No escribir en el color buffer

            // Setear el test de stencil y las funciones
            glStencilFunc(GL_NEVER, 0x00, 0xFF);
            glStencilOp(GL_INCR, GL_KEEP, GL_KEEP);

            glFrontFace(GL_CCW);   // Orientación para el espejo

#ifdef USE_AUX_STACK
            pushMatrix();
#else
            glPushMatrix();
#endif
            {
                // Aplicar la transformación para el Model Space del espejo
                mirrors[i].applyMirrorT();
                mirrors[i]
                    .drawMirrorSurface();   // Dibujar el espejo (sin marco)

                glFrontFace(
                    GL_CW);   // Orientación inversa para la escena reflejada

                mirrors[i]
                    .applyReflectionT();   // Aplicar la reflexión del espejo

                if (max_recursions) {   // Si al menos hay un nivel de recursión
                    redrawStencils(mirrors, i, 2);   // Lanzar la recursión
                }
            }
#ifdef USE_AUX_STACK
            pushMatrix();
#else
            glPopMatrix();
#endif
        }
        glPopAttrib();
    }
}

/**
 * Función recursiva que dibuja toda la escena y los diferentes rebotes de los
 * reflejos según un vector con todos los espejos, el espejo en el cual se está
 * reflejado la escena y un índice para el nivel de recusión necesario para la
 * condición de corte
 * @param mirrors Vector de espejos que componen la escena
 * @param current Índice dentro del vector del espejo actual en el cuál se está
 * reflejando
 * @param recursion_lvl Nivel de recursión actual
 */
void drawMirroredScene(Mirrors mirrors, size_t current, int recursion_lvl) {
    int cullfacing = recursion_lvl % 2;   // Orientación para el espejo

    for (size_t i = 0; i < mirrors.size();
         ++i) {   // Para cada espejo en la escena
        if (i == current)
            continue;   // Si es el actual, no se refleja a sí mismo
        glPushAttrib(GL_ALL_ATTRIB_BITS);
        {
            // Setear el test de stencil y las funciones para armar la máscara
            glStencilFunc(GL_NEVER, 0x01, 0xFF);
            glStencilOp(GL_INCR, GL_KEEP, GL_KEEP);

            glFrontFace(GL_CW + cullfacing);   // Orientación para el espejo

#ifdef USE_AUX_STACK
            pushMatrix();
#else
            glPushMatrix();
#endif
            {
                // Aplicar la transformación para el Model Space del espejo
                mirrors[i].applyMirrorT();
                // Dibujar el espejo (sin marco)
                mirrors[i].drawMirrorSurface();

                // Activar el plano de clipping y la ecuación
                // Los coeficientes se transforman al View Space automáticamente
                glEnable(GL_CLIP_PLANE0);
                glClipPlane(GL_CLIP_PLANE0, &clipplingPlane[0]);

                glPushAttrib(GL_ALL_ATTRIB_BITS);
                {
                    // Dibujar solo en el espejo
                    glStencilFunc(GL_EQUAL, recursion_lvl, 0xFF);
                    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

                    // Orientación inversa para la escena reflejada
                    glFrontFace(GL_CCW - cullfacing);

#ifdef USE_AUX_STACK
                    pushMatrix();
#else
                    glPushMatrix();
#endif
                    {
                        // Aplicar la reflexión del espejo
                        mirrors[i].applyReflectionT();

                        if (recursion_lvl <
                            max_recursions) {   // Si no se llegó al nivel
                                                // máximo
                            // Lanzar otro nivel de recursión
                            drawMirroredScene(mirrors, i, recursion_lvl + 1);
                        }

                        drawScene();   // Dibujar la escena reflejada por el
                                       // espejo actual
                    }
#ifdef USE_AUX_STACK
                    pushMatrix();
#else
                    glPopMatrix();
#endif
                }
                glPopAttrib();

                // Setear el test de stencil para deshacer la máscara del
                // espejo y además actualizar el z-buffer con la ubicación
                // del espejo, para evitar renderizar objetos detrás
                glStencilFunc(GL_LEQUAL, recursion_lvl, 0xFF);
                glStencilOp(GL_DECR, GL_DECR, GL_DECR);

                // Desactivar el clipping plane (genera z-figthing)
                glDisable(GL_CLIP_PLANE0);

                glColorMask(GL_FALSE, GL_FALSE, GL_FALSE,
                            GL_FALSE);   // No escribir en el color buffer

                // Dibujar el espejo para decrementar y restaurar el stencil
                // buffer para el nivel anterior, además de actualizar el
                // z-buffer
                mirrors[i].drawMirrorSurface();
            }
#ifdef USE_AUX_STACK
            pushMatrix();
#else
            glPopMatrix();
#endif
        }
        glPopAttrib();
    }
}

/**
 * Función que dibuja toda la escena. Además, en el caso de que haya 2 o más
 * espejos y se haya definido al menos 1 nivel de recursión máximo, lanza la
 * misma para generar los rebotes de reflejos
 */
void drawAllScene(void) {
    for (size_t i = 0; i < mirrors.size();
         ++i) {   // Para cada espejo en la escena
        glPushAttrib(GL_ALL_ATTRIB_BITS);
        {
            // Setear el test de stencil y las funciones para armar la máscara
            glStencilFunc(GL_NEVER, 0x01, 0xFF);
            glStencilOp(GL_INCR, GL_KEEP, GL_KEEP);

            glFrontFace(GL_CCW);   // Orientación para el espejo

#ifdef USE_AUX_STACK
            pushMatrix();
#else
            glPushMatrix();
#endif
            {
                // Aplicar la transformación para el Model Space del espejo
                mirrors[i].applyMirrorT();
                // Dibujar el espejo (sin marco)
                mirrors[i].drawMirrorSurface();

                // Activar el plano de clipping y la ecuación
                // Los coeficientes se transforman al View Space automáticamente
                glEnable(GL_CLIP_PLANE0);
                glClipPlane(GL_CLIP_PLANE0, &clipplingPlane[0]);

                glPushAttrib(GL_ALL_ATTRIB_BITS);
                {
                    // Dibujar solo en el espejo
                    glStencilFunc(GL_EQUAL, 0x01, 0xFF);
                    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

                    // Orientación inversa para la escena reflejada
                    glFrontFace(GL_CW);

#ifdef USE_AUX_STACK
                    pushMatrix();
#else
                    glPushMatrix();
#endif
                    {
                        // Aplicar la reflexión del espejo
                        mirrors[i].applyReflectionT();

                        if (mirrors.size() - 1 &&
                            max_recursions) {   // Si al menos hay dos espejos y
                            // al menos un nivel de recursión
                            drawMirroredScene(mirrors, i,
                                              2);   // Lanzar la recursión
                        }

                        drawScene();   // Dibujar la escena reflejada por el
                                       // espejo actual
                    }
#ifdef USE_AUX_STACK
                    pushMatrix();
#else
                    glPopMatrix();
#endif
                }
                glPopAttrib();

                // Setear el test de stencil para deshacer la máscara del
                // espejo y además actualizar el z-buffer con la ubicación
                // del espejo, para evitar renderizar objetos detrás
                glStencilFunc(GL_ALWAYS, 0x01, 0xFF);
                glStencilOp(GL_KEEP, GL_DECR, GL_DECR);

                // Desactivar el clipping plane (genera z-figthing)
                glDisable(GL_CLIP_PLANE0);

                glColorMask(GL_FALSE, GL_FALSE, GL_FALSE,
                            GL_FALSE);   // No escribir en el color buffer

                // Dibujar el espejo para decrementar y actualizar el z-buffer
                mirrors[i].drawMirrorSurface();
            }
#ifdef USE_AUX_STACK
            pushMatrix();
#else
            glPopMatrix();
#endif
        }
        glPopAttrib();
    }

    // Limpiar el buffer de stencil
    // glClear(GL_STENCIL_BUFFER_BIT);

    drawScene();   // Dibujar la escena (posición original)
}

void changeScene(char sceneNumber) {
    switch (sceneNumber) {
        case '1':
            mirrors.clear();
            mirrors.push_back(mirror1);
            mirrors.push_back(mirror2);
            mirrors.push_back(mirror3);
            max_recursions = 3;
            break;

        case '2':
            mirrors.clear();
            mirrors.push_back(mirror4);
            mirrors.push_back(mirror5);
            max_recursions = 5;
            break;

        case '3':
            mirrors.clear();
            mirrors.push_back(mirror1);
            mirrors.push_back(mirror6);
            max_recursions = 5;
            break;

        case '4':
            mirrors.clear();
            mirrors.push_back(mirror7);
            mirrors.push_back(mirror8);
            max_recursions = 5;
            break;
    }
}

void initialize_scene(void) {
    // Cargar la malla de la mona
    susan = new Malla(SUSAN_MESH);

    // Inicializar en la escena 1
    changeScene('1');
}

#endif   // __DRAW_FUNC_H__