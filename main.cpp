#include <GL/gl.h>
#include <GL/glut.h>

#include "draw-func.h"
#include "stencil.h"

#include "camera-cb.h"
#include "aux-cb.h"

extern int w, h;   // tamaño de la ventana

float lAmbient[] = {.2, .2, .2},   // luz ambiente
    lDiffuse[] = {.7, .7, .7},     // luz difusa
    lSpecular[] = {.9, .9, .9};    // luz especular

void initialize() {
    glutInitDisplayMode(GLUT_DEPTH | GLUT_RGBA | GLUT_DOUBLE | GLUT_STENCIL);

    // Ventana principal
    glutInitWindowSize(w, h);
    glutInitWindowPosition(50, 50);
    glutCreateWindow("TP final");

    // Callbacks
    glutDisplayFunc(Display_cb);
    glutReshapeFunc(Reshape_cb);
    glutKeyboardFunc(Keyboard_cb);   // teclado
    glutSpecialFunc(Special_cb);     // teclas especiales
    glutMouseFunc(Mouse_cb);         // botones picados
    glutIdleFunc(Idle_cb);

    // crea el menu
    createMenu();

    // Deshabilitar mezcla
    glDisable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Habilitar el z-buffer en modo "normal"
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);            // Interpolar las normales por nodo
    glPolygonMode(GL_FRONT, GL_FILL);   // Caras frontales y rellenas
    glEnable(GL_POLYGON_OFFSET_FILL);   // Polygon offset
    glPolygonOffset(1., 1.);

    glEnable(GL_LINE_SMOOTH);

    // Cullfacing
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    // Inicializar la iluminación de la escena
    // Habilitar la iluminación
    glEnable(GL_LIGHTING);

    glLightfv(GL_LIGHT0, GL_AMBIENT, lAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lDiffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lSpecular);

    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

    glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHT0);

    // Inicializar escena
    initialize_scene();

    // Color de fondo
    glClearColor(.8, .8, .8, 1);

    // Stencil test y Stencil buffer
    glEnable(GL_STENCIL_TEST);
    glClearStencil(0);   // Limpiar el buffer (setear todo en 0)
    glStencilOp(GL_KEEP, GL_KEEP,
                GL_KEEP);   // Deshabilitar la escritura en el buffer
    glStencilFunc(GL_ALWAYS, 0x00, 0x00);   // Pasa siempre
}

int main(int argc, char** argv) {
    // Inicializar el GLUT
    glutInit(&argc, argv);

    // Inicializar los parámetros básicos y crear la ventana principal
    initialize();

    // Loop de eventos
    glutMainLoop();

    return 0;
}
